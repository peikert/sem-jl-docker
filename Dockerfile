FROM rocker/verse:4.1.1
ARG CRAN="https://packagemanager.rstudio.com/cran/__linux__/focal/2021-10-29"
ARG JULIA_VERSION=1.7.2
ARG LAVAAN_VERSION=623762a96c4da1
ARG SEM_VERSION="devel"
RUN /rocker_scripts/install_tidyverse.sh
RUN /rocker_scripts/install_julia.sh
COPY install_lavaan.sh ./
RUN /install_lavaan.sh
COPY install_sem.sh ./
RUN /install_sem.sh

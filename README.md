
<!-- README.md is generated from README.Rmd. Please edit that file -->

# StructuralEquationModells.jl Docker Container

<!-- badges: start -->

<!-- badges: end -->

We use Docker containers for testing StructuralEquationModells.jl. Our
performance tests run in an HPC environment where these images are
converted to singularity images.

The basis of our image is formed by the rocker project (specifically
[rocker-versioned2](https://github.com/rocker-org/rocker-versioned2)),
which supplies R docker containers. However, they also ship CUDA
drivers, Julia and more and follow a modular concept. All rocker images
include the folder `/rocker_scripts` at build time. If we base an image
upon a rocker image, we can add other rocker modules by running these
scripts,
e.g. [`install_julia.sh`](https://github.com/rocker-org/rocker-versioned2/blob/master/scripts/install_julia.sh).

This is than our Dockerfile:

```` r
cat("```\n", readLines("Dockerfile"), "\n```", sep = "\n")
#> ```
#> 
#> FROM rocker/r-ver:4.1.1
#> ARG CRAN="https://packagemanager.rstudio.com/cran/__linux__/focal/2021-10-29"
#> ARG JULIA_VERSION=1.7.0-rc3
#> ARG LAVAAN_VERSION=623762a96c4da1
#> ARG SEM_VERSION="devel"
#> RUN /rocker_scripts/install_tidyverse.sh
#> RUN /rocker_scripts/install_julia.sh
#> COPY install_lavaan.sh ./
#> RUN /install_lavaan.sh
#> COPY install_sem.sh ./
#> RUN /install_sem.sh
#> 
#> ```
````
